﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models.Donhang
{
    public class Donhang
    {
        public int ID { get; set; }
        public Nullable<int> ID_sanpham { get; set; }
        public Nullable<int> ID_phieu { get; set; }
        public Nullable<int> Soluong { get; set; }
        public Nullable<double> Dongia { get; set; }
        public string color { get; set; }
        public List<Donhang> getProductAt(int skip, int take)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.Phieudathangs.OrderByDescending(m => m.ID);
                Mapper.CreateMap<Phieudathang, Donhang>();
                return Mapper.Map<IQueryable<Phieudathang>, List<Donhang>>(tam);
            }
        }
    }
}