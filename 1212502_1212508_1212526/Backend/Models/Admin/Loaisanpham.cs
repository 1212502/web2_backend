﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models.Admin
{
    public class ThemloaisanphamModels
    {
        public string Loai { get; set; }
    }

    public class ThemnhasanxuatModels
    {
        public string Ten { get; set; }
    }

    public class LoaisanphamModels
    {
        public int ID { get; set; }

        public string Loai { get; set; }
    }

    public class nhasanxuatModels
    {
        public int ID { get; set; }

        public string Ten { get; set; }
    }

    public class XoaLoaisanphamModels
    {
        public int ID { get; set; }
    }

    public class XoanhasanxuatModels
    {
        public int ID { get; set; }
    }
}