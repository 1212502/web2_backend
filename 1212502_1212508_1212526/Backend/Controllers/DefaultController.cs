﻿using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route("api/layALLTinTuc")]
        public HttpResponseMessage layALLTinTuc()
        {
            TintucModel model = new TintucModel();
            var temp = model.getALLNews();
            return Request.CreateResponse(HttpStatusCode.OK,temp );
        }
        [HttpGet]
        [Route("api/getMenu")]
        public HttpResponseMessage getMenu()
        {
            MenuModel model = new MenuModel();
            var temp = model.getMenu();
            return Request.CreateResponse(HttpStatusCode.OK, temp);
        }
        [HttpGet]
        [Route("api/getSlider")]
        public HttpResponseMessage getSlider()
        {
            SlidersModel model = new SlidersModel();
            var temp = model.getSlider();
            return Request.CreateResponse(HttpStatusCode.OK, temp);
        }
        [HttpGet]
        [Route("api/getIndex")]
        public HttpResponseMessage getIndex()
        {
            IndexModel model = new IndexModel();
            var temp = model.getIndexInfor();
            return Request.CreateResponse(HttpStatusCode.OK, temp);
        }
        
    }
}
