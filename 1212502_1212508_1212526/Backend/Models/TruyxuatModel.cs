﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class TruyxuatModel
    {
        public string image { get; set; }
        public string Title { get; set; }
        public string srcPage { get; set; }
        public string noiDung { get; set; }
        public async Task<List<TruyxuatModel>> trichXuatThongTin()
        {
            try
            {
                var config = Configuration.Default.WithDefaultLoader();
                var address = "http://genk.vn/mobile.chn";
                var document = await BrowsingContext.New(config).OpenAsync(address);
                var cells = document.QuerySelectorAll("div.list-news");

                List<TruyxuatModel>ds = new List<TruyxuatModel>();
                for (int i = 0; i < cells.ToList().Count; i++ )
                {
                    var image = cells[i].QuerySelector("img");
                    var urlima = image.GetAttribute("src");
                    var title = image.GetAttribute("title");
                
                    var div = cells[i].QuerySelector("div.list-news-img");
                    var href = div.FirstElementChild.GetAttribute("href");
                    var content = cells[i].QuerySelectorAll("p").ToList();
                    string nd = "";
                    if(content.Count == 2)
                    {
                        nd = content[1].InnerHtml;
                    }
                    TruyxuatModel temp = new TruyxuatModel()
                    {
                        image = urlima,
                        Title = title,
                        srcPage = "http://genk.vn" + href,
                        noiDung = nd
                    };
                    ds.Add(temp);
                }
                return ds;
            }
            catch(Exception e){
                return new List<TruyxuatModel>();
            }
        }
    }
}