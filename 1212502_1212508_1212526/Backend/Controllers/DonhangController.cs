﻿using Backend.Models;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DonhangController : ApiController
    {
        [HttpGet]
        [Route("api/Donhang/layALLDonhang")]
        public HttpResponseMessage layALLSanPham()
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getALLProduct());
        }

    }
}