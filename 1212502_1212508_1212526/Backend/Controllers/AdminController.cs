﻿using Backend.Models;
using Backend.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    [RoutePrefix("api/admin")]
    public class AdminController : ApiController
    {
        [HttpPost]
        [Route("LoaiSanPham/Add")]
        public HttpResponseMessage AddLoaiSanPham(ThemloaisanphamModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                db.LoaiSanPhams.Add(new LoaiSanPham() { TenLoai = model.Loai });
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("LoaiSanPham/Edit")]
        public HttpResponseMessage EditLoaiSanPham(LoaisanphamModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var loaisp = db.LoaiSanPhams.Where(m => m.ID == model.ID).First();
                loaisp.TenLoai = model.Loai;
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("LoaiSanPham/Delete")]
        public HttpResponseMessage XoaLoaiSanPham(XoaLoaisanphamModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var loaisp = db.LoaiSanPhams.Where(m => m.ID == model.ID).First();
                db.LoaiSanPhams.Remove(loaisp);
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //[HttpPost]
        //[Route("Sanpham/Add")]
        //public HttpResponseMessage ThemSanPham(Object model)
        //{
        //    //using (EcommicEntities db = new EcommicEntities())
        //    //{
        //    //    var loaisp = db.LoaiSanPhams.Where(m => m.ID == model.ID).First();
        //    //    db.LoaiSanPhams.Remove(loaisp);
        //    //    db.SaveChanges();
        //    //}
        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}

        [HttpPost]
        [Route("Nhasanxuat/Add")]
        public HttpResponseMessage AddNhasanxuat(ThemnhasanxuatModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                db.NSXes.Add(new NSX() { Name = model.Ten });
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("Nhasanxuat/Edit")]
        public HttpResponseMessage EditNhasanxuat(nhasanxuatModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var nsx = db.NSXes.Where(m => m.ID == model.ID).First();
                nsx.Name = model.Ten;
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("Nhasanxuat/Delete")]
        public HttpResponseMessage XoaNhasanxuat(XoanhasanxuatModels model)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var nsx = db.NSXes.Where(m => m.ID == model.ID).First();
                db.NSXes.Remove(nsx);
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
