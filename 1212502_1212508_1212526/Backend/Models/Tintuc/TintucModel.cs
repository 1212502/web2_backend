﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class TintucModel
    {
        public int ID { get; set; }
        public string Tittle { get; set; }
        public string Author { get; set; }
        public string Imageurl { get; set; }
        public Nullable<System.DateTime> Ngay { get; set; }
        public string Noidung { get; set; }
        public List<TintucModel> getALLNews()
        {
            using(EcommicEntities db = new EcommicEntities())
            {
                List<Tintuc> ds = new List<Tintuc>();
                var tam = db.Tintucs;
                Mapper.CreateMap<Tintuc, TintucModel>();
                return Mapper.Map<IQueryable<Tintuc>, List<TintucModel>>(tam);
            }
        }
    }
}