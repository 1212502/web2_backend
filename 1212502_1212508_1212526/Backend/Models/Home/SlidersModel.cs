﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class SlidersModel
    {
        public List<Slider> getSlider()
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                return db.Sliders.ToList();
            }
        }
    }
}