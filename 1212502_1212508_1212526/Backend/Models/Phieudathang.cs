//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backend.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Phieudathang
    {
        public Phieudathang()
        {
            this.Chitietphieux = new HashSet<Chitietphieu>();
        }
    
        public int ID { get; set; }
        public Nullable<int> Nguoimua { get; set; }
        public Nullable<int> Nguoinhan { get; set; }
        public Nullable<System.DateTime> Ngaymua { get; set; }
        public string Ghichu { get; set; }
        public Nullable<int> Trangthai { get; set; }
        public Nullable<int> Soluong { get; set; }
        public Nullable<double> Tongtien { get; set; }
        public Nullable<int> tinhTrang { get; set; }
    
        public virtual ICollection<Chitietphieu> Chitietphieux { get; set; }
        public virtual Nguoinhan Nguoinhan1 { get; set; }
        public virtual TaiKhoan TaiKhoan { get; set; }
    }
}
