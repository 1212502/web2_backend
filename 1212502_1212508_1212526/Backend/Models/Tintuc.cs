//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Backend.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tintuc
    {
        public Tintuc()
        {
            this.Comments = new HashSet<Comment>();
        }
    
        public int ID { get; set; }
        public string Tittle { get; set; }
        public string Author { get; set; }
        public string Imageurl { get; set; }
        public Nullable<System.DateTime> Ngay { get; set; }
        public string Noidung { get; set; }
    
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
