﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class MenuModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<SubMenuModel> subMenu { get; set; }

        public List<MenuModel>getMenu()
        {
            using(EcommicEntities db = new EcommicEntities())
            {
                var menu = db.Menus;
                List<MenuModel> dsMenu = new List<MenuModel>();
                foreach(var item in menu)
                {
                    MenuModel temp = new MenuModel();
                    temp.ID = item.ID;
                    temp.Name = item.Name;
                    temp.subMenu = (from p in db.Submenus where p.Id_menu == item.ID select new SubMenuModel() { ID = p.ID, Name = p.Name,Action = p.Action }).ToList();
                    dsMenu.Add(temp);
                }
                return dsMenu;
            }
        }
    }
    public class SubMenuModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Id_menu { get; set; }
        public string Action { get; set; }
    }
}