﻿using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SanPhamController : ApiController
    {
        [HttpGet]
        [Route("api/sanpham/layALLSanPham")]
        public HttpResponseMessage layALLSanPham()
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK,model.getALLProduct());
        }
        [HttpGet]
        [Route("api/sanpham/laySanPhamAt")]
        public HttpResponseMessage layALLSanPham([FromUri]int skip,[FromUri]int take)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK,model.getProductAt(skip,take));
        }
        [HttpGet]
        [Route("api/sanpham/loaiSanPham")]
        public HttpResponseMessage loaiSanPham()
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getTypeProduct());
        }
        [HttpGet]
        [Route("api/sanpham/sanphamtheotype")]
        public HttpResponseMessage sanPhamTheoLoai(string type,int skip,int take)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getProductforType(Int32.Parse(type),skip,take));
        }
        [HttpGet]
        [Route("api/sanpham/layALLSanPhamTheoLoai")]
        public HttpResponseMessage layALLSanPhamType(string type)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getAllProductforType(Int32.Parse(type)));
        }
        [HttpGet]
        [Route("api/sanpham/laymotsanpham")]
        public HttpResponseMessage laymotsanpham(int IDSP)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getaProduct(IDSP));
        }
        [HttpGet]
        [Route("api/sanpham/AllNSX")]
        public HttpResponseMessage NSX()
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.getNSX());
        }
        [HttpGet]
        [Route("api/sanpham/TimkiemFor")]
        public HttpResponseMessage Timkiem(string ID_Loai,string nameNSX,string nameProduct)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.SearchProduct(ID_Loai,nameNSX,nameProduct));
        }
        [HttpGet]
        [Route("api/sanpham/TimkiemNangcao")]
        public HttpResponseMessage Timkiem1(string name)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.SearchProduct1(name));
        }
        [HttpGet]
        [Route("api/sanpham/Sanphamlienquan")]
        public HttpResponseMessage spLienquan(string ID)
        {
            SanphamModel model = new SanphamModel();
            return Request.CreateResponse(HttpStatusCode.OK, model.Sanphamlienquan(ID));
        }

    }
}
