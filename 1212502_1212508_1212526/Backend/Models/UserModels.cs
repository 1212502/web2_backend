﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class Information
    {
        public string UserName { get; set; }

        public string HoTen { get; set; }

        public string Gioitinh { get; set; }

        public DateTime? Namsinh { get; set; }

        public string Diachi { get; set; }

        public string SDT { get; set; }

        public string Anhdaidien { get; set; }

        public DateTime? Ngaydangki { get; set; }

        public string Email { get; set; }
    }
}