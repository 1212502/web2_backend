﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TruyxuatController : ApiController
    {
        [HttpGet]
        [Route("api/RSS")]
        public async Task<IEnumerable<TruyxuatModel>> RSSFIT()
        {
            TruyxuatModel rss = new TruyxuatModel();          
            return await rss.trichXuatThongTin();
        }
    }
}
