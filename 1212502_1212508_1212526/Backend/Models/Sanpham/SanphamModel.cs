﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class SanphamModel
    {
        public int ID { get; set; }
        public string TenSanPham { get; set; }
        public string Duongdanhinh { get; set; }
        public Nullable<double> Giatien { get; set; }
        public string Motasanpham { get; set; }
        public Nullable<int> ID_Loai { get; set; }
        public Nullable<int> Soluong { get; set; }
        public int NSX { get; set; }
        public string TenNSX { get; set; }
        public Nullable<System.DateTime> Ngaydang { get; set; }
        public List<ColorModel> Color { get; set; }

        public List<SanphamModel>getProductAt(int skip,int take)
        {
            using(EcommicEntities db = new EcommicEntities())
            {
                var tam = db.Sanphams.OrderByDescending(m => m.Ngaydang).Skip(skip).Take(take);
                return mapvagetNSX(tam, db);
            }
        }
        public List<SanphamModel> getALLProduct()
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.Sanphams.OrderByDescending(m => m.Ngaydang);
                return mapvagetNSX(tam, db);
            }
        }
        public List<LoaiSanPhamModel> getTypeProduct()
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.LoaiSanPhams;
                Mapper.CreateMap<LoaiSanPham, LoaiSanPhamModel>();
                return Mapper.Map<IQueryable<LoaiSanPham>, List<LoaiSanPhamModel>>(tam);
            }
        }
        public List<SanphamModel> getProductforType(int type,int skip,int take)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.Sanphams.Where(m => m.ID_Loai == type).OrderByDescending(m => m.TenSanPham).Skip(skip).Take(take);
                return mapvagetNSX(tam, db);
            }
        }
        public List<SanphamModel> SearchProduct(string ID_Loai, string nameNSX, string nameProduct)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                try
                {
                    List<SanphamModel> tam = getALLProduct();
                    if(!string.IsNullOrEmpty(ID_Loai))
                    {
                        int id_loai = Int32.Parse(ID_Loai);
                        tam = tam.Where(m => m.ID_Loai == id_loai).ToList();
                    }
                    if (!string.IsNullOrEmpty(nameNSX))
                    {
                        tam = tam.Where(m => m.TenNSX.Equals(nameNSX)).ToList();
                    }
                    if (!string.IsNullOrEmpty(nameProduct))
                    {
                        tam = tam.Where(m => m.TenSanPham.Contains(nameProduct)).ToList();
                    }

                    return tam;
                }
                catch (Exception e)
                {
                    return null;
                }
                
            }
        }

        
        // lay tat ca san pham theo loai
        public List<SanphamModel> getAllProductforType(int type)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.Sanphams.Where(m => m.ID_Loai == type);
                
                return mapvagetNSX(tam,db);
            }
        }

        private List<SanphamModel> mapvagetNSX(IQueryable<Sanpham> tam, EcommicEntities db)
        {
            Mapper.CreateMap<Sanpham, SanphamModel>();
            var mapSP = Mapper.Map<IQueryable<Sanpham>, List<SanphamModel>>(tam);
            foreach (var item in mapSP)
            {
                string NSX = (from p in db.NSXes where p.ID == item.NSX select new { name = p.Name }).FirstOrDefault().name;
                item.TenNSX = NSX;
            }
            return mapSP;
        }
        // Lay san pham theo id
        public SanphamModel getaProduct(int ID)
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                SanphamModel model = new SanphamModel();
                var tam = db.Sanphams.Find(ID);

                Mapper.CreateMap<Sanpham, SanphamModel>();

                model = Mapper.Map<Sanpham, SanphamModel>(tam);
                Mapper.CreateMap<Color, ColorModel>();
                model.Color = Mapper.Map<ICollection<Color>,List<ColorModel>>(tam.Colors);
                model.TenNSX = tam.NSX1.Name;
                return model;
            }
        }
        // Lay Tất cả NSX
        public List<NSXModel> getNSX()
        {
            using (EcommicEntities db = new EcommicEntities())
            {
                var tam = db.NSXes;
                Mapper.CreateMap<NSX, NSXModel>();
                return Mapper.Map<IQueryable<NSX>, List<NSXModel>>(tam);
            }
        }


        internal List<SanphamModel> SearchProduct1(string name)
        {
            using(EcommicEntities db = new EcommicEntities())
            {
                var sp = (from p in db.Sanphams where p.TenSanPham.Contains(name) || p.NSX1.Name.Contains(name) || p.LoaiSanPham.TenLoai.Contains(name) select p);
                return mapvagetNSX(sp,db);
            }
        }

        internal List<SanphamModel> Sanphamlienquan(string ID)
        {
            try
            {
                using(EcommicEntities db = new EcommicEntities())
                {
                    var sp = db.Sanphams.Find(ID);
                    var ds = db.Sanphams.Where(m => m.Giatien < (sp.Giatien + 2000000) && m.Giatien > (sp.Giatien - 2000000)).ToList();
                    if( ds.Count < 3)
                    {
                        ds = db.Sanphams.Where(m=>m.NSX== sp.NSX).ToList();
                    }
                    Mapper.CreateMap<Sanpham, SanphamModel>();
                    return Mapper.Map<List<Sanpham>, List<SanphamModel>>(ds);
                }
            }
            catch (Exception e) { return new List<SanphamModel>(); }
        }
    }
    /// <summary>
    /// class loai san pham
    /// </summary>
    public class LoaiSanPhamModel
    {
        public int ID { get; set; }
        public string TenLoai { get; set; }
    }
    /// <summary>
    /// Class color
    /// </summary>
    public class ColorModel
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public Nullable<int> ID_sanpham { get; set; }
    }
    public class NSXModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}