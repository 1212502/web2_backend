﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class IndexModel
    {
        public List<Slider> Sliders;
        public List<SanphamModel> sanPhamMoi;
        public List<SanphamModel> sanPham;
        public List<LoaiSanPhamModel> loaiSanPham;

        public IndexModel getIndexInfor()
        {
            IndexModel model = new IndexModel();
            SlidersModel modelsliders = new SlidersModel();
            SanphamModel modelSanpham = new SanphamModel();
            model.Sliders = modelsliders.getSlider();
            model.sanPhamMoi = modelSanpham.getProductAt(0, 3);
            model.sanPham = modelSanpham.getProductAt(3, 5);
            model.loaiSanPham = modelSanpham.getTypeProduct();
            return model;
        }
    }
}